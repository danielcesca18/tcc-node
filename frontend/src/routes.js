import React from 'react';

import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import { isAuthenticated } from "./services/auth";

import RegisterFase from './pages/registerFase/registerFase';
import LoginUser from './pages/loginUser/loginUser';
import RegisterUser from './pages/registerUser/registerUser';
import Menu from './pages/menu/menu';
import Play from './pages/jogar/jogar';
import Galeria from './pages/galeria/galeria';
import ConfigUser from './pages/configUser/configUser';
import EditFase from './pages/editFase/editFase';

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      render={props =>
        isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
      }
    />
  );

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={LoginUser} />
            <Route path='/register' component={RegisterUser} />
            <PrivateRoute path='/menu' component={Menu} />
            <PrivateRoute path='/registerfase' component={RegisterFase} />
            <PrivateRoute path='/play' component={Play} />
            <PrivateRoute exact path='/fases' component={Galeria} />
            <PrivateRoute path='/user' component={ConfigUser} />
            <PrivateRoute path='/fases/:id' component={EditFase} />
        </Switch>
    </BrowserRouter>
);

export default Routes;