import React, { useEffect,useState } from 'react';
import { toast } from 'react-toastify';

import './jogar.css'
import api from '../../services/api';

//pagina carrega e bota as fases no localstorage
//set fases bota na fases
//removerpela retorna tudo menos a fase removida

export default function Play(){
  const [fases, setFases] = useState([]);

  useEffect(() => {
    async function loadFases(){
      const user_id = localStorage.getItem('user');
      const response = await api.get('/fase/play', {
        headers: { user_id }
      });
      
      //const fase = localStorage.getItem('fase');

      //if(response.data._id === fase){ console.log("deu certo!") };
      
      setFases(response.data);
    }
    loadFases();
  }, []);

  async function handleOption(e) {
    const correct = fases.correct;
    if(e === correct){
      toast.success("Acertou!");

      // const fase = [localStorage.getItem('fase')];
      // fase.push(fases._id);

      // localStorage.setItem('fase', fase);

      setTimeout(function() {
        window.location.reload();
    }, 3000);

    }else{
      toast.error("Errou!");
    }
  };

  return (
        <div className='login-page'>
            <h1>Daily.me</h1>
            <div className="form-style">
            <ul className="play-items">
                    <li>
                      <header style={{ backgroundImage: `url(${fases.img_url})`}} />
                      <h2>{fases.title}</h2>
                      <h3>{fases.subtitle}</h3>
                      </li>
                      <li>
                      <ul className="teste">
                        <button onClick={e => handleOption('option1')}>{fases.op1}</button>
                        <button onClick={e => handleOption('option2')}>{fases.op2}</button>
                        <button onClick={e => handleOption('option3')}>{fases.op3}</button>
                        <button onClick={e => handleOption('option4')}>{fases.op4}</button>
                      </ul>
                    </li>
            </ul>
            </div>
        </div>
  )
}
