import React, { Component } from 'react';
import api from "../../services/api";

import '../loginUser/login.css'

export default class LoginUser extends Component {
  state = {
    name: "",
    email: "",
    password: "",
    error: ""
  };

  handleSignUp = async e => {
    e.preventDefault();
    
    const { name, email, password } = this.state;
  if (!name || !email || !password) {
    this.setState({ error: "Preencha todos os campos" });
    alert(this.state.error);
  } else {
    try {
      await api.post("/user/register", { name, email, password });
      alert("Conta registrada com sucesso!");
      this.props.history.push("/");
    } catch (err) {
      console.log(err);
      this.setState({ error: "Ocorreu um erro ao registrar sua conta" });
      alert(this.state.error);
    }
  }

  }
  
  render() {
    return (
        <div className='login-page'>
            <h1>Daily.me</h1>
            <form onSubmit={this.handleSignUp}>
                <input 
                  type='text' 
                  placeholder="Nome"
                  onChange={e => this.setState({ name: e.target.value })}
                />
                <input 
                  type='email' 
                  placeholder="E-mail"
                  onChange={e => this.setState({ email: e.target.value })}
                />
                <input 
                  type='password' 
                  placeholder="Senha"
                  onChange={e => this.setState({ password: e.target.value })}
                />
                <button type='submit'>registrar</button>
                <p className="message">Já possui uma conta? <a href="/">Entrar</a></p>
            </form>
        </div>
    );
  }
}
