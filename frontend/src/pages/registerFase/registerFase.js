import React, { Component } from 'react';
import api from "../../services/api";

import '../loginUser/login.css'

export default class LoginUser extends Component {
  state = {
    title: "",
    subtitle: "",
    img: null,
    op1:"",
    op2:"",
    op3:"",
    op4:"",
    correct: "",
    error: ""
  };

  handleOptionChange = e =>{
    this.setState({
      correct: e.target.value
    });
  }

  handleSignUp = async e => {
        e.preventDefault();
        
        const { title, subtitle, img, op1, op2, op3, op4, correct} = this.state;
        if (!title || !subtitle || !img || !op1 || !op2 || !op3 || !op4  || !correct) {
           await this.setState({ error: "Preencha todos os campos" });
            alert(this.state.error);
        } else {
        try {
          const formData = new FormData();
          formData.append('title',this.state.title);
          formData.append('subtitle',this.state.subtitle);
          formData.append('img',this.state.img);
          formData.append('op1',this.state.op1);
          formData.append('op2',this.state.op2);
          formData.append('op3',this.state.op3);
          formData.append('op4',this.state.op4);
          formData.append('correct',this.state.correct);

          const user_id = await localStorage.getItem('user');

          const config = {
            headers: { user_id }
          };

            await api.post("/fase/register", formData, config);
            alert("Fase cadastrada com sucesso!");        
            window.location.reload();
        } catch (err) {
            await this.setState({ error: "Ocorreu um erro ao registrar a fase" });
            alert(this.state.error);
        }
    }

  }
  
  render() {
    return (
        <div className='login-page'>
            <h1>Daily.me</h1>
            <form onSubmit={this.handleSignUp}>
                <input 
                  type='text' 
                  placeholder="Título"
                  onChange={e => this.setState({ title: e.target.value })}
                />
                <input 
                  type='text' 
                  placeholder="Descrição"
                  onChange={e => this.setState({ subtitle: e.target.value })}
                />
                <input 
                  type='file' 
                  onChange={e => this.setState({ img: e.target.files[0] })}
                />

                <input 
                  type='text' 
                  placeholder="Opção 1"
                  onChange={e => this.setState({ op1: e.target.value })}
                />
                <input type="radio" value="option1" 
                  checked={this.state.correct === 'option1'} 
                  onChange={this.handleOptionChange} 
                />

                <input 
                  type='text' 
                  placeholder="Opção 2"
                  onChange={e => this.setState({ op2: e.target.value })}
                />
                <input type="radio" value="option2" 
                  checked={this.state.correct === 'option2'} 
                  onChange={this.handleOptionChange} 
                />

                <input 
                  type='text' 
                  placeholder="Opção 3"
                  onChange={e => this.setState({ op3: e.target.value })}
                />
                <input type="radio" value="option3" 
                  checked={this.state.correct === 'option3'} 
                  onChange={this.handleOptionChange} 
                />

                <input 
                  type='text' 
                  placeholder="Opção 4"
                  onChange={e => this.setState({ op4: e.target.value })}
                />
                <input type="radio" value="option4" 
                  checked={this.state.correct === 'option4'} 
                  onChange={this.handleOptionChange} 
                />


                <button type='submit'>registrar</button>
            </form>
        </div>
    );
  }
}
