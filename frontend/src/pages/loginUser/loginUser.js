import React, { Component } from 'react';

import api from "../../services/api";
import { login } from "../../services/auth";

import './login.css';

export default class LoginUser extends Component {
  state = {
    email: '',
    password: '',
    error: ''
  }

  handleSubmit = async e => {
    e.preventDefault();

    const { email , password } = this.state;

    if (!email.length || !password.length){
      this.setState({ error: "Preencha e-mail e senha para continuar!" });
      alert(this.state.error);
    }else{
      try{
        const response = await api.post("/user/authenticate", { email, password });

        const { _id } = await response.data.user;
        localStorage.setItem('user', _id);

        login(response.data.token);

        this.props.history.push("/menu");
      } catch (err) {
        await this.setState({error:"Error ao logar, verifique suas credenciais."});
        alert(this.state.error);
      }
    }
  }

  render() {
    return (
        <div className='login-page'>
            <h1>Daily.me</h1>
            <form onSubmit={this.handleSubmit}>
                <input 
                  type='email'
                  placeholder="E-mail"
                  value={this.state.email}
                  onChange={e => this.setState({ email: e.target.value })}
                />
                <input 
                  type='password' 
                  placeholder="Senha"
                  value={this.state.password}
                  onChange={e => this.setState({ password: e.target.value })}
                />
                <button type="submit">Login</button>
                <p className="message">Não possui uma conta? <a href="register">Registre-se</a></p>
                <p className="message"><a href="forgotpassword">Esqueci minha senha</a></p>
            </form>
        </div>
    );
  }
}
