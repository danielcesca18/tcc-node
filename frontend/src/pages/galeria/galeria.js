import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import api from '../../services/api';
import '../loginUser/login.css'

export default function Galeria(){
    const[fases, setFases] = useState([]);

    useEffect(() => {
        async function loadFases(){
            const user_id = localStorage.getItem('user');
            const response = await api.get('/fase/list', {
              headers: { user_id }
            });
      
            setFases(response.data);
          }

        loadFases();
    }, []);

    return(
        <div className='login-page'>
            <h1>Daily.me</h1>
        <form>
            <ul className="fase-list">
                {fases.map(fases => (
                    <li key={fases._id}>
                      <header style={{ backgroundImage: `url(${fases.img_url})`}} />
                      <strong>{fases.title}</strong>
                      <Link to={`fases/${fases._id}`}><button>Editar</button></Link>
                    </li>
                ))}
            </ul>
        </form>
        </div>
    )
}

