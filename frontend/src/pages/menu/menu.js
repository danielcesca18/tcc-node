import React, { Component } from 'react';
import './menu.css';

import { logout } from "../../services/auth";

export default class pages extends Component {

  Handlelogout = () =>{
    logout();
    this.props.history.push("/");
  }

  render() {
    return (
      <div className='container'>
        <div className='menu'>
          <h1>Daily.me</h1>
          <div className='box'>
            <section className="stage">
              <div className="div"><a href="play"><p className="p">Iniciar</p></a></div>
              <div className="div"><a href="registerfase"><p className="p">Criar Fase</p></a></div>
              <div className="div"><a href="fases"><p className="p">Fases</p></a></div>
              <div className="div"><a href = "user"><p className="p">Usuário</p></a></div>
              <div className="div" onClick={this.Handlelogout}><p className="p">Sair</p></div>
            </section>
          </div>
      </div>
      </div>
    );
  }
}
