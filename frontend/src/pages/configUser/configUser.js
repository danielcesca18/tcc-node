import React, { Component } from 'react';
import api from "../../services/api";
import { logout } from "../../services/auth";

import '../loginUser/login.css'

const user_id = localStorage.getItem('user');

export default class LoginUser extends Component {
  state = {
    name: "",
    email: "",
    password: "",
    newPassword: "",
    error: ""
  };

  Handlelogout = () =>{
    logout();
    this.props.history.push("/");
  }

  deleteAccount = async e => {
    e.preventDefault();

    if(window.confirm('Deseja deletar sua conta?')){

      const { email, password } = this.state;

      if (!password || !email) {
        await this.setState({ error: "Informe suas credenciais!" });
        alert(this.state.error);
      }else{
        try {
          await api.delete(`/user/delete/${user_id}`);
          await alert('Conta deletada com sucesso!');
          this.Handlelogout();
      } catch (err) {
          console.log(err);
          this.setState({ error: "Ocorreu um erro ao deletar conta" });
          await alert(this.state.error);
      }
      }

    }
  }

  handleSignUp = async e => {
    e.preventDefault();
    
    const { name, email, password, newPassword } = this.state;

    if (!password || !email || !name) {
        await this.setState({ error: "Informe suas credenciais!" });
        alert(this.state.error);
    }else{
        try {
            await api.put(`/user/update/${user_id}`, { name, email, password, newPassword });
            this.props.history.push("/menu");
        } catch (err) {
            console.log(err);
            this.setState({ error: "Ocorreu um erro ao atualizar sua conta" });
            alert(this.state.error);
        }
      }

  }
  
  render() {
    return (
        <div className='login-page'>
            <h1>Daily.me</h1>
            <form onSubmit={this.handleSignUp}>
                <input 
                  type='email' 
                  placeholder="E-mail*"
                  onChange={e => this.setState({ email: e.target.value })}
                />
                <input 
                  type='password' 
                  placeholder="Senha*"
                  onChange={e => this.setState({ password: e.target.value })}
                />
                <hr />
                <input 
                  type='text' 
                  placeholder="Novo Nome"
                  onChange={e => this.setState({ name: e.target.value })}
                />
                <input 
                  type='password' 
                  placeholder="Nova Senha"
                  onChange={e => this.setState({ newPassword: e.target.value })}
                />
                <button type='submit'>Atualizar perfil</button>
                <p className="message">Preenchimento dos campos obrigatório!*</p>
                <p className="message-red"><a href='?' onClick={this.deleteAccount}>Deletar usuário</a></p>
            </form>
        </div>
    );
  }
}
