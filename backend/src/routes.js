//imports
const express = require ('express');
const routes = express.Router();
const multer = require ('multer');
const uploadFile = require ('./middleware/uploadFile');

const upload = multer(uploadFile);

//importando controlladores
const userController = require ('./controllers/userController');
const faseController = require ('./controllers/faseController');

//Users - rotas CRUD
routes.post('/user/authenticate', userController.authenticate) //autenticar
routes.get('/user/list', userController.index);                //listar todos
routes.post('/user/register', userController.register);        //registrar
routes.get('/user/show/:id', userController.show);             //listar um
routes.put('/user/update/:id', userController.update);         //atualizar
routes.delete('/user/delete/:id', userController.delete);      //deletar


//Fases - rotas CRUD
routes.get('/fase/list', faseController.index);                //listar todos
routes.get('/fase/play', faseController.play);                //listar todos
routes.post('/fase/register', upload.single('img'), faseController.register);        //register
routes.get('/fase/show/:id', faseController.show);             //listar um
routes.put('/fase/update/:id', upload.single('img'), faseController.update);         //atualizar
routes.delete('/fase/delete/:id', faseController.delete);      //deletar

//exports
module.exports = routes;