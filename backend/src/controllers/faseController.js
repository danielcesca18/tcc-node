//imports
const Fase = require ('../models/fase');
const multer = require('../middleware/uploadFile');

//exportando ações
module.exports = {

    //jogar
    async play(req,res){
        try{
            const { user_id } = req.headers;
            const fases = await Fase.find( {createdBy: user_id});

            const random = Math.floor(Math.random() * fases.length);

            return res.json(fases[random]);
        } catch (error) {
            console.error('Erro ao Listar Todos');
        }
    },

    //listar todas as fase
    async index(req,res){
        try{
            const { user_id } = req.headers;
            const fases = await Fase.find( {createdBy: user_id});

            return res.json(fases);
        } catch (error) {
            console.error('Erro ao Listar Todos');
        }
    },

    //listar fase
    async show(req,res){
        try{
            const fase = await Fase.findById(req.params.id);

            return res.json(fase);

        } catch (error) {
            console.error('Erro ao Listar Fase');
        }
    },

    //registrar usuario
    async register(req,res){
        try{
            const { filename } = await req.file;
            const {title, subtitle, op1, op2, op3, op4, correct} = req.body;
            const { user_id } = req.headers;
            const fase = await Fase.create({
                createdBy: user_id,
                img: filename,
                title, subtitle, op1, op2, op3, op4, correct
            });
            return res.json(fase);

        } catch (error) {
            console.error('Error Register-----', error);
        }
    },

    //atualizar fase
    async update(req,res){
        try{
            const { filename } = await req.file;
            const {title, subtitle, op1, op2, op3, op4, correct} = req.body;
            const fase = await Fase.findByIdAndUpdate(req.params.id, {
                title, subtitle, op1, op2, op3, op4, correct, img: filename },
                { new: true });
            return res.json(fase);

        } catch (error) {
            console.error('Erro ao Atualizar Fase');
        }
    },

    //deletar usuario
    async delete(req,res){
        try{
            const fase = await Fase.findByIdAndRemove(req.params.id);

            return res.json(fase);

        } catch (error) {
            console.error('Erro ao Deletar Fase');
        }
    },

};