//imports
const User = require ('../models/user');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const authConfig = require ('../config/auth');

//gerador de token
function generateToken(params = {}){
    //retorna um token, gerado baseado na "secret" que expira em 1 dia
    return jwt.sign({ params }, authConfig.secret, {
        expiresIn: 20000,
    });
}


//exportando ações
module.exports = {

    //listar todos os usuarios
    async index(req,res){
        try{
            const user = await User.find();

            return res.json(user);

        } catch (error) {
            console.error('Erro ao Listar Todos');
        }
    },

    //listar usuario
    async show(req,res){
        try{
            const user = await User.findById(req.params.id);

            return res.json(user);

        } catch (error) {
            console.error('Erro ao Listar Usuário');
        }
    },

    //registrar usuario
    async register(req,res){
        const { email } = req.body;

        try{
            //registro com email já cadastrado
            if (await User.findOne({ email })){
                return res.status(400).send({ error: 'Usuário já existente' });
            }

            const user = await User.create(req.body);

            //não retornar o email de volta para o usuário
            //user.password = undefined;
            
            //retorna user
            return res.json(user);

        } catch (error) {
            console.error('Erro ao Registrar Usuário'); 
        }
    },

    //atualizar usuario
    async update(req,res){
        //*****ERRO: -- FAZER VERIFICAÇÃO SE O EMAIL INSERIDO EXISTE*****/
        const { email, newPassword, password } = req.body;
        try{
            const user = await User.findOne({ email }).select('+password');
            //verifica senha
            if (!await bcrypt.compare(password, user.password)){
                console.error(error);
                return res.status(400).send({ error: 'Senha inválida' });
            }else{
                const hash = await bcrypt.hash(newPassword, 10);
                req.body.password = hash;

                const user = await User.findByIdAndUpdate(req.params.id, req.body, { new: true });  
                return res.json(user);
            }

        } catch (error) {
            console.error('Erro ao Atualizar Usuário');
            console.error(error);
        }
    },

    //deletar usuario
    async delete(req,res){
        try{
            const user = await User.findByIdAndRemove(req.params.id);

            return res.json(user);

        } catch (error) {
            console.error('Erro ao Deletar Usuário');
        }
    },

     //autenticar usuario
     async authenticate(req,res){
        const { email, password } = req.body;

        try{
            //busca email e senha
            const user = await User.findOne({ email }).select('+password');

            //verifica existencia do user
            if(!user){
                return res.status(400).send({ error: 'Usuário não encontrado' });
            }

            //verifica senha
            if (!await bcrypt.compare(password, user.password)){
                return res.status(400).send({ error: 'Senha inválida' });
            }

            //user.password = undefined;

            //retorna user e token
            return res.json({ 
                user, 
                token: generateToken ({ id: user.id }),
            });


        } catch(error){
            console.error('Erro ao Autenticar');
        }
    },

};
