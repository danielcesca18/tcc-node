//imports
const express = require ('express');
const mongoose = require ('mongoose');
const cors = require ('cors');
const path = require('path');

const app = express();

//conexão banco
mongoose.connect('mongodb+srv://user_bd:user123@bd-teste-4rbm4.mongodb.net/test?retryWrites=true&w=majority', 
    { 
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
        
    }, function(err){
        if (err) { 
            return console.error('Erro ao conectar com o Banco de Dados');
        }
    }
);

//utulitários
app.use(cors());
app.use(express.json());
app.use(require('./routes'));
app.use('/files', express.static(path.resolve(__dirname, '..', 'images')));

//porta da api
app.listen(3001, () => {
    console.log('Server iniciado na porta 3001...');
});