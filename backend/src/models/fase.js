//imports
const mongoose = require('mongoose');

//Schema da Fase
const FaseSchema = new mongoose.Schema({
    createdBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    title: {
        type: String,
    },
    subtitle: {
        type: String,
    },
    img: {
        type: String,
    },
    op1: {
        type: String,
    },
    op2: {
        type: String,
    },
    op3: {
        type: String,
    },
    op4: {
        type: String,
    },
    correct: {
        type: String,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
}, {
    toJSON: {
        virtuals: true,
    },
});

FaseSchema.virtual('img_url').get(function(){
    return `http://localhost:3001/files/${this.img}`
})

//exports
module.exports = mongoose.model('Fase', FaseSchema);